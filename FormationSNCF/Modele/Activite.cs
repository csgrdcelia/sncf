﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace FormationSNCF.Modele
{
    [Serializable]
    /// <summary>
    /// Classe métier : représente une activité
    /// </summary>
    public class Activite
    {
        private string _libelleActivite;
        private List<ActionFormation> _lesActionsFormation;

        /// <summary>
        /// Obtient le libellé de l'activité
        /// </summary>
        public string LibelleActivite
        {
            get { return _libelleActivite; }
        }

        /// <summary>
        /// Initialise une activité
        /// </summary>
        /// <param name="libelleActivite">Le libellé de l'activité</param>
        public Activite(string libelleActivite)
        {
            _libelleActivite = libelleActivite;
            _lesActionsFormation = new List<ActionFormation>();
        }

        /// <summary>
        /// Initialise une action de formation et l'ajoute à sa collection
        /// </summary>

        public void AjouterActionFormation(string unCodeAction, string unIntituleDeFormation, decimal unCout, int uneDuree, DateTime uneDate)
        {
            ActionFormation uneActionDeFormation = new ActionFormation(unCodeAction, unIntituleDeFormation, unCout, uneDuree, uneDate);
            _lesActionsFormation.Add(uneActionDeFormation);
        }

        /// <summary>
        /// Recherche une action de formation de l'activité à partir d'un code d'action de formation
        /// </summary>

        public ActionFormation ObtenirActionFormation(string unCodeAction)
        {
            ActionFormation actionFormation = null;
            foreach (ActionFormation actionFormationCourante in _lesActionsFormation)
            {
                if (actionFormationCourante.CodeAction == unCodeAction)
                {
                    actionFormation = actionFormationCourante;
                    break;
                }

            }
            return actionFormation;

        }

        /// <summary>
        /// Supprime une action de formation de l'activité à partir d'un code d'action de formation
        /// </summary>
        
        public bool SupprimerActionFormation(string codeAction)
        {
            return _lesActionsFormation.Remove(ObtenirActionFormation(codeAction));
        }

        /// <summary>
        /// Permet l'accès en lecture à l'ensemble des actions de formation de l'activité
        /// </summary>
        
        public ReadOnlyCollection<ActionFormation> ActionFormation
        {
            get
            {
                return new ReadOnlyCollection<ActionFormation>(_lesActionsFormation);
            }
        }

        /// <summary>
        /// Compare les activités
        /// </summary>

        public override bool Equals (object uneActivite)
        {
            bool egal = false;
            Activite activiteComparee = uneActivite as Activite;
            if(activiteComparee != null)
            {
                if (activiteComparee.LibelleActivite == _libelleActivite)
                    egal = true;
            }
            return egal;
        }

        /// <summary>
        /// Permet d'effectuer des recherches dans un ensemble d'occurences de même type
        /// </summary>

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Permet de retourner le libellé de l'activité
        /// </summary>

        public override string ToString()
        {
            return _libelleActivite;
        }




    }
}
