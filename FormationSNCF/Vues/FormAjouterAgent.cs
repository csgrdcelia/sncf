﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Vues;
using FormationSNCF.Ressources;
using System.Text.RegularExpressions;


namespace FormationSNCF.Vues
{
    public partial class FormAjouterAgent : Form
    {
        public FormAjouterAgent()
        {
            InitializeComponent();
            MinimumSize = Size;
            MaximumSize = Size;
        }

        private bool VerifFormulaireAjoutAgent()
        {
            bool resultat = false;

            if (this.comboBoxCivilite.SelectedIndex <= 0)
            {
                MessageBox.Show("Sélectionner la civilité de l'agent");
            }
            else if (this.textBoxNom.Text == "")
            {
                MessageBox.Show("Saisir le nom de l'agent");
            }
            else if (this.textBoxPrenom.Text == "")
            {
                MessageBox.Show("Saisir le prénom de l'agent");
            }
            else
            {
                resultat = true;
            }
            return resultat;
        }
        
        private void TextBoxCodePostal_Leave(object sender, EventArgs e)
        {
            if (textBoxCodePostal.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatCodePostal(textBoxCodePostal.Text))
                {
                    MessageBox.Show("Saisir un code postal à 5 chiffres");
                    textBoxCodePostal.Text = "";
                    textBoxCodePostal.Focus();
                }
            }
        }

        private void TextBoxDateNaissance_Leave(object sender, EventArgs e)
        {
            if (textBoxDateNaissance.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatDate(textBoxDateNaissance.Text))
                {
                    MessageBox.Show("Format : jj-mm-aaaa ou jj/mm/aaaa");
                    textBoxDateNaissance.Text = "";
                    textBoxDateNaissance.Focus();
                }
            }
        }

        private void TextBoxDateEmbauche_Leave(object sender, EventArgs e)
        {
            if (textBoxDateNaissance.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatDate(textBoxDateEmbauche.Text))
                {
                    MessageBox.Show("Format : jj-mm-aaaa ou jj/mm/aaaa");
                    textBoxDateEmbauche.Text = "";
                    textBoxDateEmbauche.Focus();
                }
            }
        }


    }
}